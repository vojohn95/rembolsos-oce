<?php

namespace Database\Factories;

use App\Models\PagoAprovado;
use Illuminate\Database\Eloquent\Factories\Factory;

class PagoAprovadoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PagoAprovado::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_aprueba_usuarios' => $this->faker->word,
        'tipo_pago' => $this->faker->word,
        'monto_pago' => $this->faker->word,
        'fecha_pago' => $this->faker->word,
        'id_estacionamiento_paga' => $this->faker->randomDigitNotNull,
        'id_solicitud_fondo' => $this->faker->randomDigitNotNull,
        'folio_anticipo' => $this->faker->word,
        'observaciones' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
