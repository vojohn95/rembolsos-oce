<?php

namespace Database\Factories;

use App\Models\SolicitudFondo;
use Illuminate\Database\Eloquent\Factories\Factory;

class SolicitudFondoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SolicitudFondo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_estacionamiento' => $this->faker->randomDigitNotNull,
        'tipo_solicitud' => $this->faker->word,
        'concepto' => $this->faker->word,
        'importe_solicitado' => $this->faker->word,
        'fecha_ingreso_finanzas' => $this->faker->word,
        'fecha_entrega_cxp' => $this->faker->word,
        'fecha_ingreso_cxp' => $this->faker->word,
        'estatus_registro' => $this->faker->word,
        'estatus_peticion' => $this->faker->word,
        'observaciones' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
