<?php

namespace Database\Factories;

use App\Models\Estacionamiento;
use Illuminate\Database\Eloquent\Factories\Factory;

class EstacionamientoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Estacionamiento::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'no_estacionamiento' => $this->faker->randomDigitNotNull,
        'nombre_proyecto' => $this->faker->word,
        'direccion' => $this->faker->word,
        'calle' => $this->faker->word,
        'no_ext' => $this->faker->word,
        'colonia' => $this->faker->word,
        'municipio' => $this->faker->word,
        'estado' => $this->faker->word,
        'pais' => $this->faker->word,
        'cp' => $this->faker->word,
        'tipo_estacionamiento' => $this->faker->word,
        'id_gerentes' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'estatus' => $this->faker->word
        ];
    }
}
