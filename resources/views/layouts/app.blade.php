<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
          integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
          crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap4-toggle/3.6.1/bootstrap4-toggle.min.css"
          integrity="sha512-EzrsULyNzUc4xnMaqTrB4EpGvudqpetxG/WNjCpG6ZyyAGxeB6OBF9o246+mwx3l/9Cn838iLIcrxpPHTiygAA=="
          crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- AdminLTE -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.1.0/css/adminlte.min.css"
          integrity="sha512-mxrUXSjrxl8vm5GwafxcqTrEwO1/oBNU25l20GODsysHReZo4uhVISzAKzaABH6/tTfAxZrY2FprmeAP5UZY8A=="
          crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- iCheck -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/icheck-bootstrap/3.0.1/icheck-bootstrap.min.css"
          integrity="sha512-8vq2g5nHE062j3xor4XxPeZiPjmRDh6wlufQlfC6pdQ/9urJkU07NM0tEREeymP++NczacJ/Q59ul+/K2eYvcg=="
          crossorigin="anonymous"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
          integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
          crossorigin="anonymous"/>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          integrity="sha512-aEe/ZxePawj0+G2R+AaIxgrQuKT68I28qh+wgLrcAJOz3rxCP+TwrK5SPN+E5I+1IQjNtcfvb96HDagwrKRdBw=="
          crossorigin="anonymous"/>
          <link rel="stylesheet"
          href="//cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css"/>

    @stack('third_party_stylesheets')

    @stack('page_css')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    <!-- Main Header -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown user-menu">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBIQEhMREhISFBIXERIRGBsREhIUEhISFxMYHBgYFxcbISwkGx0pHhoXJTYpKS4wNTMzGiI5PjkyPSwyMzABCwsLEA4QHRISHjIqJCAyMjIyMjIyMjIyMjIyMDIyMjIwMjIyMjIyMjIyMjIyMjIyMDIyMjIyMjIyMjIyMjIyMv/AABEIAN0A5AMBIgACEQEDEQH/xAAbAAEAAwEBAQEAAAAAAAAAAAAABAUGAQMCB//EAEMQAAIBAQQECwYDBAsBAAAAAAABAgMEBRFREiExQQYyUmFxcoGRobHREyJCU5LBFWKTFiNDczM0Y4KDorPC0uHwFP/EABoBAQACAwEAAAAAAAAAAAAAAAABAwIEBgX/xAAxEQACAQICBgoCAwEBAAAAAAAAAQIDEQQxBRIhQVGREyIyYXGBsdHh8KHxQlLBFRT/2gAMAwEAAhEDEQA/APzcAGRAAAAAAAAAAAAABKs931anEpyazfux72WNHg9UfHqU49XFv7EpNmvVxVGlslJevoUgNNDg/TXGnVfRo4eR7xuOzrdN9M39jLUZqPS2HWV35e7MkDXfgln5L+uXqec7hoPY6i6Jr74jUZitL0OD5L3MqDRVODsfgqyXXSflgQq9xVo8XRmvySePc8CNVmxDSGHnlLnsKoH3VpSg8JxlF5STXmfBibiae1AAAkAAAAAAAAAAAAAAAAAAAAAAkWSyTrS0YLF738MVm3uNPd91Qo4Sfv1OU1s6uRkotmnisdTw627XwX+8CksVyVKmEp/u4/mXvvoju7S9st1UaWDUNKXKqe8+zcuwmgtUEjncRj61bY3ZcF92nTgBmaQAAAAAAAAB81KcZrRlFSWUkmvEqbXcFOeum9B5P+jf3RcAxaTL6OIqUXeDt6cjFWuxVKLwqRaya1xfQyMb2pBSTjJJp6mmsUzP3lcbjjOji1tcHra6r39BVKDWR7uF0rGp1aux8d3wUQAMD1wAAAAAAAAAAAAAAATbtu+deWC1QXGllzLNnLvsUq89FaorXJ8lepr6FGNOKhBYRWz/ANmZRjc8zSGP6BakO0/x88F5s+bNZ4UoqEFgvFvNvez1ALzmZScnd5sAAkxAB5V7RTprGc1HrPW+hbyCYpydlmeoKqrf9GPFVSfQkl4+hFlwjypd8/8Aox10bkNH4mX8Odl6svwZ5cI5fKh9TPWnwij8VKS6rUvPAdJEyejMSv4/le5eAgUL4oT+PReVRKPjs8SfF461rXNsMk0zUnSnTdpprxAAJKwAACrva6Y1cZwwjU8J9PPzmXnBxbjJNNPBp7UzeFZfF2KstOCwqpfWsnz5FU4b0exo/SDp2p1H1dz4fHp4GUB1rDUcKjowAAAAAAAAAfdGnKclCKxk3gj4NJwdsejF1pLXLVHmjvfb9uclK7NbFYhUKbm893e/uZZWCxxo01GO3bJ8qW9kgA2ErHHzm5ycpZsAAkxB5Wm1U6UdKpJJbs5cyW8jXneUaCw1SqNao5c8ubzMraK86knObbk/BZJbkVynbI9PBaOlX689kfy/Du7yxtl+1J4qH7uOe2o+3d2d5Uzk28W23m3i32nAVN3Oio0KdFWgrAAEFwAAAJFltlSk8YTcebbF9K2EcAxlFSVmro0thv2E8I1UoS5S/o308kuTAlndl7So+7LGVPLfHq+hZGfE8bF6KT61Hl7exqweVltNOrHSpyUl4rpW49S08GUXF2eaAAJMSg4QXf8AxoLrpeE/UoDeyipJxaxTTTT2NMxt42V0akobtsXnF7PTsKJqzudHorFa8eilnHLw+PQiAAwPYAAAAAAPex2d1akKa3vXzR3vuNtCCilFLBJJLmS2FFwYs/Hqv+UvOX+0vi6mtlzmdLV9erqLKPr92AAFh5QIl5W5UIaW2T1RWbzfMiVKSSbbwSTbb2JIxt42t1qjm+Lsiso+u8rnKyPQ0fhOnqdbsxz7+77uI9WrKcnKTbk3i2958AFJ1SVtiAABIAAAAAAAAAAAB9QqSi9KLcXnFtMsaN+V46m4z/mRbfesCsBKbWRVUo06vbin95l4uEU99Kn2OSO/tHL5UO9lECdZmv8A87Df09fcvf2jl8pfU/QhXleX/wBCjjTUXFvWm3qe1bOgrwQ5NmVPBUKclKEbNd79wACDbAAAAB90Yacox5Uox73gCL2zNhdVHQo0479HTfTJY/cljDLYDZSsjiKk3OTk97vzAAJMCp4RWnQpKmts3g/5e/7LvMwWN/VtOvJboJU134vxb7itNebuzrtH0eioRW97X5/AABiboAAAAAAAAAAOpY7PAA4Dui8n3DReT7gQcB3ReT7hovJ9wBwH1ovJnMHkAcB3B5AA4AASAAACXdMca9Jf2kX3PH7EQn3L/WKfTL/TkSsynEO1Gb7n6M14ANk4oHUcOTep9VkEmGtE9Oc5ZzlLvZ5gGsdylZWAABIAAAAAALG7rqqV/e4tPlPf1cz0ua6/avTmv3af1vLozNSkksEsEtWrYkZxhfazyMfpHon0dPtb3w+fQg2a6qNPZBSlnVwk+7YuwnJYbNXQAXJJHP1Kk6jvNt+IxO4nACsAAkk6DhTX3eehjSpv33qk18KyXP5GLdi6hQlXmoQ/XeR78vPHGlTerZNrf+Rc2fcUQBQ3c63D4eFCChD9gAEF4AAAJ1zPC0Uulrvi0QSRYZ6NWnLKpDu0liSsyqvHWpyXFP0NsDpw2TiQGsdQOkAwLWGrLUcJV5UtCtVj+eT7JPFeZFNY7mEteKkt+0AAGQAAALG6btdeWMsVTT1vlPko87su+VeeGyC4zyWS5zX0KcYRUYrCKWCSM4wueVpDHqitSD6z/HzwOwgopRikklgktiR0AvOabuADgIOgAgXAPmpVjBaUpKKzk0kUt4X8knGjrfLa1LqJ7e0hySNihhqld2gvPdzJF83n7Jezg17Vr6Fn05GWbx8xKTbbbbbeLb1ts4UN3OpwmFjh4aqz3viAAQbQAAAAAAOnAAbqhV9pCM18UIy70ehV8Hq+nS0d8JNdm1fddhaGxF3VzisRT6KpKHBv7yAAMikzvCWz4ThVWyS0X1ls8PIoza3hZfbUpU9+2PNJbPTtMXKLTaawaeDx3NFE1ZnUaLr9JR1d8fTd7HAAYHpgAAHvStVSC0YVJxW3CMmlifX4hW+bU/UkRgSVulB7XFcl7En8QrfNqfqSH4hW+bU/UkRgQOip/wBVyXsSfxCt82p+pI6rwrfNqfXIigXI6Gn/AFXJexK/Eq3zan1sSt9Z7a1XsqSXkRQLk9DT/quSPqc3J4ybbzk233s+QAZgAAkAAEAAAkAAAAAAs7gtPs6ui+LNez/vPi+naaswUXhr2PyZs7ttarU4z+Liy5pLb69pbTe48DTGHtJVVv2P/CUAC08MGe4QXfg/bQWp8fDc90/U0IlFNNNYprB47GjGSurGzhcRKhUU1596MCC0ve63RenBN02+lweT5smVZQ1Y62lVhVgpwexgAEFoAAAAAAAAAAOwi5NJJtt4JLW2wDhMst2VauuMMI8qXux7N77C7uu5owwnUSlPalthD1ZcFihxPFxWllF6tJX73l5fbeJnafB2XxVYrqpvxbR6fs5H5r+hepegz6OJ5r0niX/L8L2KNcHY/Nl3L1PK2XLClCU3Oo8FqWEdcnsXeaEznCK16UlSWyOuXPNrUuxeZjKMUjZweJxVeqoa7tm8svuwpAAVHRgAAAAAAsbmt3samEn+7lgpc35uwrgMiurTjUg4SyZvgUdw3lilRm9a1Qb3rkdOReGxF3OPxFCVCbhL9oAAyKBJJpppNNYNPWmjPXjcbWM6OtbdDeuo9/QaEGLimbGHxVShK8H4rczBSi08GmmtWD1NHDa2uwUq3Hjr5S1SXbv7SmtPB6a10pKSylql37H4FTg0dBQ0pRqdrqvvy5/oowSq1gq0+NSn2JyXetRGeraYHoxnGSvF38DgB9wpSlxYyl1U35AyezM+AWFC568/g0VnUxj4bfAtrLcEI66knN5LVH1fgZKLZp1sfQpZyu+C2/BQ2Sx1Kz0accc29UY9LNRdt2QoLHjVN8mtnNHJE2nTjBKMUoxWxJYJHSyMLHg4vSM6/VWyPDj4v/AACw84AHxWqxpxc5PCKWLZBKV3ZEe8rYqFNy+J6orOXojHSk2228W22297e0k3jbHXqOb1Jaorkx9SIUSlc6zAYT/z09vaeft5eoABibwAAAAAAAAATw19vQam571VVKE3hNbHuqL1MsE8Na2821Ep2NXFYWGIhqyzWT4G+BR3XfaeEKzwexT3Pr5PnLwvUrnK4jDzoS1Zr2YABkUAAAANJ7Un0gEBHyqcV8MO5H2cAJe3MAAkgAAAAHjarVClHSnLBbs5PJLeQzKMXJ2irtnpUmoJyk0opYtvYkZS9rydeWCxUE9S3yebPm8bynXfJprZH7vNkAplK+w6TAaP6Hrz7Xp894ABgeqAAAAAAAAAAAAAAACwu69alHCPHp8lvZ1civAyK6lKFSOrNXRtLHeFOsvcl73Jnqkuzf2EowSZZ2W+6tPU2qkf7THS7JeuJaqnE8TEaHa20X5P3NUCrs9+0pcbSpvnS0e9FhSrwnxJwl1ZJmakmeTUw9Wl24teR6AAyKQAAADpErXjRhxqkMcovSfciG0jOEJTdoq/htJQk0li2ks3qSKK08IVspU8eeqtX0r1Ke1W2pV4821ktUV2IwdRHo0NE1pvr9Vc3yL63X7CGMaWE5Z/w1/yM7aK86knOcnJ8+7mS3I8gVSbZ7uGwlLDrqLbx3/HkAAQbQAAAAAAAAAAAAAAAAAAAAAAAAOnAASIWyrHi1ai/wASWHce0b2tC/ivt0X5oggm7KnRpyziuSLD8YtHzP8ALD0POd513tq1Ox4eRDAuyFh6SyguSPSpVlPjSlLrSb8zzAILVs2IAAEgAAAAAAAAAAAAAAH/2Q=="
                         class="user-image img-circle elevation-2" alt="User Image">
                    <span class="d-none d-md-inline">{{ Auth::user()->name }}</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <!-- User image -->
                    <li class="user-header bg-primary">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBIQEhMREhISFBIXERIRGBsREhIUEhISFxMYHBgYFxcbISwkGx0pHhoXJTYpKS4wNTMzGiI5PjkyPSwyMzABCwsLEA4QHRISHjIqJCAyMjIyMjIyMjIyMjIyMDIyMjIwMjIyMjIyMjIyMjIyMjIyMDIyMjIyMjIyMjIyMjIyMv/AABEIAN0A5AMBIgACEQEDEQH/xAAbAAEAAwEBAQEAAAAAAAAAAAAABAUGAQMCB//EAEMQAAIBAQQECwYDBAsBAAAAAAABAgMEBRFREiExQQYyUmFxcoGRobHREyJCU5LBFWKTFiNDczM0Y4KDorPC0uHwFP/EABoBAQACAwEAAAAAAAAAAAAAAAABAwIEBgX/xAAxEQACAQICBgoCAwEBAAAAAAAAAQIDEQQxBRIhQVGREyIyYXGBsdHh8KHxQlLBFRT/2gAMAwEAAhEDEQA/APzcAGRAAAAAAAAAAAAABKs931anEpyazfux72WNHg9UfHqU49XFv7EpNmvVxVGlslJevoUgNNDg/TXGnVfRo4eR7xuOzrdN9M39jLUZqPS2HWV35e7MkDXfgln5L+uXqec7hoPY6i6Jr74jUZitL0OD5L3MqDRVODsfgqyXXSflgQq9xVo8XRmvySePc8CNVmxDSGHnlLnsKoH3VpSg8JxlF5STXmfBibiae1AAAkAAAAAAAAAAAAAAAAAAAAAAkWSyTrS0YLF738MVm3uNPd91Qo4Sfv1OU1s6uRkotmnisdTw627XwX+8CksVyVKmEp/u4/mXvvoju7S9st1UaWDUNKXKqe8+zcuwmgtUEjncRj61bY3ZcF92nTgBmaQAAAAAAAAB81KcZrRlFSWUkmvEqbXcFOeum9B5P+jf3RcAxaTL6OIqUXeDt6cjFWuxVKLwqRaya1xfQyMb2pBSTjJJp6mmsUzP3lcbjjOji1tcHra6r39BVKDWR7uF0rGp1aux8d3wUQAMD1wAAAAAAAAAAAAAAATbtu+deWC1QXGllzLNnLvsUq89FaorXJ8lepr6FGNOKhBYRWz/ANmZRjc8zSGP6BakO0/x88F5s+bNZ4UoqEFgvFvNvez1ALzmZScnd5sAAkxAB5V7RTprGc1HrPW+hbyCYpydlmeoKqrf9GPFVSfQkl4+hFlwjypd8/8Aox10bkNH4mX8Odl6svwZ5cI5fKh9TPWnwij8VKS6rUvPAdJEyejMSv4/le5eAgUL4oT+PReVRKPjs8SfF461rXNsMk0zUnSnTdpprxAAJKwAACrva6Y1cZwwjU8J9PPzmXnBxbjJNNPBp7UzeFZfF2KstOCwqpfWsnz5FU4b0exo/SDp2p1H1dz4fHp4GUB1rDUcKjowAAAAAAAAAfdGnKclCKxk3gj4NJwdsejF1pLXLVHmjvfb9uclK7NbFYhUKbm893e/uZZWCxxo01GO3bJ8qW9kgA2ErHHzm5ycpZsAAkxB5Wm1U6UdKpJJbs5cyW8jXneUaCw1SqNao5c8ubzMraK86knObbk/BZJbkVynbI9PBaOlX689kfy/Du7yxtl+1J4qH7uOe2o+3d2d5Uzk28W23m3i32nAVN3Oio0KdFWgrAAEFwAAAJFltlSk8YTcebbF9K2EcAxlFSVmro0thv2E8I1UoS5S/o308kuTAlndl7So+7LGVPLfHq+hZGfE8bF6KT61Hl7exqweVltNOrHSpyUl4rpW49S08GUXF2eaAAJMSg4QXf8AxoLrpeE/UoDeyipJxaxTTTT2NMxt42V0akobtsXnF7PTsKJqzudHorFa8eilnHLw+PQiAAwPYAAAAAAPex2d1akKa3vXzR3vuNtCCilFLBJJLmS2FFwYs/Hqv+UvOX+0vi6mtlzmdLV9erqLKPr92AAFh5QIl5W5UIaW2T1RWbzfMiVKSSbbwSTbb2JIxt42t1qjm+Lsiso+u8rnKyPQ0fhOnqdbsxz7+77uI9WrKcnKTbk3i2958AFJ1SVtiAABIAAAAAAAAAAAB9QqSi9KLcXnFtMsaN+V46m4z/mRbfesCsBKbWRVUo06vbin95l4uEU99Kn2OSO/tHL5UO9lECdZmv8A87Df09fcvf2jl8pfU/QhXleX/wBCjjTUXFvWm3qe1bOgrwQ5NmVPBUKclKEbNd79wACDbAAAAB90Yacox5Uox73gCL2zNhdVHQo0479HTfTJY/cljDLYDZSsjiKk3OTk97vzAAJMCp4RWnQpKmts3g/5e/7LvMwWN/VtOvJboJU134vxb7itNebuzrtH0eioRW97X5/AABiboAAAAAAAAAAOpY7PAA4Dui8n3DReT7gQcB3ReT7hovJ9wBwH1ovJnMHkAcB3B5AA4AASAAACXdMca9Jf2kX3PH7EQn3L/WKfTL/TkSsynEO1Gb7n6M14ANk4oHUcOTep9VkEmGtE9Oc5ZzlLvZ5gGsdylZWAABIAAAAAALG7rqqV/e4tPlPf1cz0ua6/avTmv3af1vLozNSkksEsEtWrYkZxhfazyMfpHon0dPtb3w+fQg2a6qNPZBSlnVwk+7YuwnJYbNXQAXJJHP1Kk6jvNt+IxO4nACsAAkk6DhTX3eehjSpv33qk18KyXP5GLdi6hQlXmoQ/XeR78vPHGlTerZNrf+Rc2fcUQBQ3c63D4eFCChD9gAEF4AAAJ1zPC0Uulrvi0QSRYZ6NWnLKpDu0liSsyqvHWpyXFP0NsDpw2TiQGsdQOkAwLWGrLUcJV5UtCtVj+eT7JPFeZFNY7mEteKkt+0AAGQAAALG6btdeWMsVTT1vlPko87su+VeeGyC4zyWS5zX0KcYRUYrCKWCSM4wueVpDHqitSD6z/HzwOwgopRikklgktiR0AvOabuADgIOgAgXAPmpVjBaUpKKzk0kUt4X8knGjrfLa1LqJ7e0hySNihhqld2gvPdzJF83n7Jezg17Vr6Fn05GWbx8xKTbbbbbeLb1ts4UN3OpwmFjh4aqz3viAAQbQAAAAAAOnAAbqhV9pCM18UIy70ehV8Hq+nS0d8JNdm1fddhaGxF3VzisRT6KpKHBv7yAAMikzvCWz4ThVWyS0X1ls8PIoza3hZfbUpU9+2PNJbPTtMXKLTaawaeDx3NFE1ZnUaLr9JR1d8fTd7HAAYHpgAAHvStVSC0YVJxW3CMmlifX4hW+bU/UkRgSVulB7XFcl7En8QrfNqfqSH4hW+bU/UkRgQOip/wBVyXsSfxCt82p+pI6rwrfNqfXIigXI6Gn/AFXJexK/Eq3zan1sSt9Z7a1XsqSXkRQLk9DT/quSPqc3J4ybbzk233s+QAZgAAkAAEAAAkAAAAAAs7gtPs6ui+LNez/vPi+naaswUXhr2PyZs7ttarU4z+Liy5pLb69pbTe48DTGHtJVVv2P/CUAC08MGe4QXfg/bQWp8fDc90/U0IlFNNNYprB47GjGSurGzhcRKhUU1596MCC0ve63RenBN02+lweT5smVZQ1Y62lVhVgpwexgAEFoAAAAAAAAAAOwi5NJJtt4JLW2wDhMst2VauuMMI8qXux7N77C7uu5owwnUSlPalthD1ZcFihxPFxWllF6tJX73l5fbeJnafB2XxVYrqpvxbR6fs5H5r+hepegz6OJ5r0niX/L8L2KNcHY/Nl3L1PK2XLClCU3Oo8FqWEdcnsXeaEznCK16UlSWyOuXPNrUuxeZjKMUjZweJxVeqoa7tm8svuwpAAVHRgAAAAAAsbmt3samEn+7lgpc35uwrgMiurTjUg4SyZvgUdw3lilRm9a1Qb3rkdOReGxF3OPxFCVCbhL9oAAyKBJJpppNNYNPWmjPXjcbWM6OtbdDeuo9/QaEGLimbGHxVShK8H4rczBSi08GmmtWD1NHDa2uwUq3Hjr5S1SXbv7SmtPB6a10pKSylql37H4FTg0dBQ0pRqdrqvvy5/oowSq1gq0+NSn2JyXetRGeraYHoxnGSvF38DgB9wpSlxYyl1U35AyezM+AWFC568/g0VnUxj4bfAtrLcEI66knN5LVH1fgZKLZp1sfQpZyu+C2/BQ2Sx1Kz0accc29UY9LNRdt2QoLHjVN8mtnNHJE2nTjBKMUoxWxJYJHSyMLHg4vSM6/VWyPDj4v/AACw84AHxWqxpxc5PCKWLZBKV3ZEe8rYqFNy+J6orOXojHSk2228W22297e0k3jbHXqOb1Jaorkx9SIUSlc6zAYT/z09vaeft5eoABibwAAAAAAAAATw19vQam571VVKE3hNbHuqL1MsE8Na2821Ep2NXFYWGIhqyzWT4G+BR3XfaeEKzwexT3Pr5PnLwvUrnK4jDzoS1Zr2YABkUAAAANJ7Un0gEBHyqcV8MO5H2cAJe3MAAkgAAAAHjarVClHSnLBbs5PJLeQzKMXJ2irtnpUmoJyk0opYtvYkZS9rydeWCxUE9S3yebPm8bynXfJprZH7vNkAplK+w6TAaP6Hrz7Xp894ABgeqAAAAAAAAAAAAAAACwu69alHCPHp8lvZ1civAyK6lKFSOrNXRtLHeFOsvcl73Jnqkuzf2EowSZZ2W+6tPU2qkf7THS7JeuJaqnE8TEaHa20X5P3NUCrs9+0pcbSpvnS0e9FhSrwnxJwl1ZJmakmeTUw9Wl24teR6AAyKQAAADpErXjRhxqkMcovSfciG0jOEJTdoq/htJQk0li2ks3qSKK08IVspU8eeqtX0r1Ke1W2pV4821ktUV2IwdRHo0NE1pvr9Vc3yL63X7CGMaWE5Z/w1/yM7aK86knOcnJ8+7mS3I8gVSbZ7uGwlLDrqLbx3/HkAAQbQAAAAAAAAAAAAAAAAAAAAAAAAOnAASIWyrHi1ai/wASWHce0b2tC/ivt0X5oggm7KnRpyziuSLD8YtHzP8ALD0POd513tq1Ox4eRDAuyFh6SyguSPSpVlPjSlLrSb8zzAILVs2IAAEgAAAAAAAAAAAAAAH/2Q=="
                             class="img-circle elevation-2"
                             alt="User Image">
                        <p>
                            {{ Auth::user()->name }}
                            <small>Member since {{ Auth::user()->created_at->format('M. Y') }}</small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                        <a href="#" class="btn btn-default btn-flat float-right"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Sign out
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>

    <!-- Left side column. contains the logo and sidebar -->
@include('layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content">
            @yield('content')
        </section>
    </div>

    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
            <b>Version</b> 3.1.0
        </div>
        <strong>
           Copyright &copy; 2014-2021 <a href="https://central-mx.com">OCE</a>.
        </strong>
        All rights reserved.
    </footer>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
        integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
        crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>

<!-- AdminLTE App -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.1.0/js/adminlte.min.js"
        integrity="sha512-AJUWwfMxFuQLv1iPZOTZX0N/jTCIrLxyZjTRKQostNU71MzZTEPHjajSK20Kj1TwJELpP7gl+ShXw5brpnKwEg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
        integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"
        integrity="sha512-GDey37RZAxFkpFeJorEUwNoIbkTwsyC736KNSYucu1WJWFK9qTdzYub8ATxktr6Dwke7nbFaioypzbDOQykoRg=="
        crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"
        integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A=="
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/1.3/bootstrapSwitch.min.js"
        integrity="sha512-DAc/LqVY2liDbikmJwUS1MSE3pIH0DFprKHZKPcJC7e3TtAOzT55gEMTleegwyuIWgCfOPOM8eLbbvFaG9F/cA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>

<script>
    $(function () {
        bsCustomFileInput.init();
    });

    $("input[data-bootstrap-switch]").each(function () {
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
</script>

@stack('third_party_scripts')

@stack('page_scripts')

@yield('scripts')
</body>
</html>
