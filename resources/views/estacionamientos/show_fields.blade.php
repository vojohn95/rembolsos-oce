<!-- No Estacionamiento Field -->
<div class="col-sm-12">
    {!! Form::label('no_estacionamiento', 'No Estacionamiento:') !!}
    <p>{{ $estacionamiento->no_estacionamiento }}</p>
</div>

<!-- Nombre Proyecto Field -->
<div class="col-sm-12">
    {!! Form::label('nombre_proyecto', 'Nombre Proyecto:') !!}
    <p>{{ $estacionamiento->nombre_proyecto }}</p>
</div>

<!-- Direccion Field -->
<div class="col-sm-12">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{{ $estacionamiento->direccion }}</p>
</div>

<!-- Calle Field -->
<div class="col-sm-12">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{{ $estacionamiento->calle }}</p>
</div>

<!-- No Ext Field -->
<div class="col-sm-12">
    {!! Form::label('no_ext', 'No Ext:') !!}
    <p>{{ $estacionamiento->no_ext }}</p>
</div>

<!-- Colonia Field -->
<div class="col-sm-12">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{{ $estacionamiento->colonia }}</p>
</div>

<!-- Municipio Field -->
<div class="col-sm-12">
    {!! Form::label('municipio', 'Municipio:') !!}
    <p>{{ $estacionamiento->municipio }}</p>
</div>

<!-- Estado Field -->
<div class="col-sm-12">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{{ $estacionamiento->estado }}</p>
</div>

<!-- Pais Field -->
<div class="col-sm-12">
    {!! Form::label('pais', 'Pais:') !!}
    <p>{{ $estacionamiento->pais }}</p>
</div>

<!-- Cp Field -->
<div class="col-sm-12">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{{ $estacionamiento->cp }}</p>
</div>

<!-- Tipo Estacionamiento Field -->
<div class="col-sm-12">
    {!! Form::label('tipo_estacionamiento', 'Tipo Estacionamiento:') !!}
    <p>{{ $estacionamiento->tipo_estacionamiento }}</p>
</div>

<!-- Id Gerentes Field -->
<div class="col-sm-12">
    {!! Form::label('id_gerentes', 'Id Gerentes:') !!}
    <p>{{ $estacionamiento->id_gerentes }}</p>
</div>

<!-- Estatus Field -->
<div class="col-sm-12">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $estacionamiento->estatus }}</p>
</div>

