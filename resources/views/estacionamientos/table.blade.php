<div class="table-responsive">
    <table class="table" id="estacionamientos-table">
        <thead>
        <tr>
            <th>No Estacionamiento</th>
        <th>Nombre Proyecto</th>
        <th>Direccion</th>
        <th>Calle</th>
        <th>No Ext</th>
        <th>Colonia</th>
        <th>Municipio</th>
        <th>Estado</th>
        <th>Pais</th>
        <th>Cp</th>
        <th>Tipo Estacionamiento</th>
        <th>Id Gerentes</th>
        <th>Estatus</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($estacionamientos as $estacionamiento)
            <tr>
                <td>{{ $estacionamiento->no_estacionamiento }}</td>
            <td>{{ $estacionamiento->nombre_proyecto }}</td>
            <td>{{ $estacionamiento->direccion }}</td>
            <td>{{ $estacionamiento->calle }}</td>
            <td>{{ $estacionamiento->no_ext }}</td>
            <td>{{ $estacionamiento->colonia }}</td>
            <td>{{ $estacionamiento->municipio }}</td>
            <td>{{ $estacionamiento->estado }}</td>
            <td>{{ $estacionamiento->pais }}</td>
            <td>{{ $estacionamiento->cp }}</td>
            <td>{{ $estacionamiento->tipo_estacionamiento }}</td>
            <td>{{ $estacionamiento->id_gerentes }}</td>
            <td>{{ $estacionamiento->estatus }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['estacionamientos.destroy', $estacionamiento->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('estacionamientos.show', [$estacionamiento->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('estacionamientos.edit', [$estacionamiento->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
