<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control','maxlength' => 50,'maxlength' => 50]) !!}
</div>

<!-- Clave Distrito Field -->
<div class="form-group col-sm-6">
    {!! Form::label('clave_distrito', 'Clave Distrito:') !!}
    {!! Form::text('clave_distrito', null, ['class' => 'form-control','maxlength' => 20,'maxlength' => 20]) !!}
</div>

<!-- Estatus Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('estatus', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('estatus', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('estatus', 'Estatus', ['class' => 'form-check-label']) !!}
    </div>
</div>


<!-- Correo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('correo', 'Correo:') !!}
    {!! Form::text('correo', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100]) !!}
</div>