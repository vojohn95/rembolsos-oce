<!-- Nombre Field -->
<div class="col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $distrital->nombre }}</p>
</div>

<!-- Clave Distrito Field -->
<div class="col-sm-12">
    {!! Form::label('clave_distrito', 'Clave Distrito:') !!}
    <p>{{ $distrital->clave_distrito }}</p>
</div>

<!-- Estatus Field -->
<div class="col-sm-12">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $distrital->estatus }}</p>
</div>

<!-- Correo Field -->
<div class="col-sm-12">
    {!! Form::label('correo', 'Correo:') !!}
    <p>{{ $distrital->correo }}</p>
</div>

