<!-- Id Aprueba Usuarios Field -->
{!! Form::hidden('id_aprueba_usuarios', Auth::user()->id, ['class' => 'form-control']) !!}

<!-- Tipo Pago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_pago', 'Tipo Pago:') !!}
    {!! Form::text('tipo_pago', null, ['class' => 'form-control', 'maxlength' => 255, 'maxlength' => 255]) !!}
</div>

<!-- Monto Pago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_pago', 'Monto Pago:') !!}
    {!! Form::number('monto_pago', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Pago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_pago', 'Fecha Pago:') !!}
    {!! Form::text('fecha_pago', null, ['class' => 'form-control', 'id' => 'fecha_pago']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#fecha_pago').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Id Estacionamiento Paga Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_estacionamiento_paga', 'Id Estacionamiento Paga:') !!}
    {!! Form::number('id_estacionamiento_paga', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Solicitud Fondo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_solicitud_fondo', 'Id Solicitud Fondo:') !!}
    {!! Form::number('id_solicitud_fondo', null, ['class' => 'form-control']) !!}
</div>

<!-- Folio Anticipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('folio_anticipo', 'Folio Anticipo:') !!}
    {!! Form::text('folio_anticipo', null, ['class' => 'form-control', 'maxlength' => 255, 'maxlength' => 255]) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control', 'maxlength' => 1000, 'maxlength' => 1000]) !!}
</div>
