<div class="table-responsive">
    <table class="table" id="pagoAprovados-table">
        <thead>
            <tr>
                <th>Aprueba Usuarios</th>
                <th>Tipo Pago</th>
                <th>Monto Pago</th>
                <th>Fecha Pago</th>
                <th>Estacionamiento Paga</th>
                <th>Folio Anticipo</th>
                <th>Observaciones</th>
                <th>Solicitud Fondo</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pagoAprovados as $pagoAprovado)
                <tr>
                    <td>{{ $pagoAprovado->idApruebaUsuarios->name }}</td>
                    <td>{{ $pagoAprovado->tipo_pago }}</td>
                    <td>
                        @if ($pagoAprovado->monto_pago == 0)
                            Pendiente de actualización
                        @else
                            {{ $pagoAprovado->monto_pago }}
                        @endif
                    </td>
                    <td>
                        @if (is_null($pagoAprovado->fecha_pago))
                            Pendiente de actualización
                        @else
                            {{ $pagoAprovado->fecha_pago }}
                        @endif
                    </td>
                    <td>{{ $pagoAprovado->id_estacionamiento_paga }}</td>
                    <td>{{ $pagoAprovado->folio_anticipo }}</td>
                    <td>{{ $pagoAprovado->observaciones }}</td>
                    <td>{{ $pagoAprovado->id_solicitud_fondo }} </td>
                    {!! Form::open(['route' => ['pagoAprovados.destroy', $pagoAprovado->id], 'method' => 'delete']) !!}
                    <td width="120">
                        <a href="{{ route('pagoAprovados.edit', [$pagoAprovado->id]) }}"
                            class='btn btn-primary btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </td>
                    {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#pagoAprovados-table').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
        });
    </script>
@endsection
