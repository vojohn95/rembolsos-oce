<!-- Id Aprueba Usuarios Field -->
<div class="col-sm-12">
    {!! Form::label('id_aprueba_usuarios', 'Id Aprueba Usuarios:') !!}
    <p>{{ $pagoAprovado->id_aprueba_usuarios }}</p>
</div>

<!-- Tipo Pago Field -->
<div class="col-sm-12">
    {!! Form::label('tipo_pago', 'Tipo Pago:') !!}
    <p>{{ $pagoAprovado->tipo_pago }}</p>
</div>

<!-- Monto Pago Field -->
<div class="col-sm-12">
    {!! Form::label('monto_pago', 'Monto Pago:') !!}
    <p>{{ $pagoAprovado->monto_pago }}</p>
</div>

<!-- Fecha Pago Field -->
<div class="col-sm-12">
    {!! Form::label('fecha_pago', 'Fecha Pago:') !!}
    <p>{{ $pagoAprovado->fecha_pago }}</p>
</div>

<!-- Id Estacionamiento Paga Field -->
<div class="col-sm-12">
    {!! Form::label('id_estacionamiento_paga', 'Id Estacionamiento Paga:') !!}
    <p>{{ $pagoAprovado->id_estacionamiento_paga }}</p>
</div>

<!-- Id Solicitud Fondo Field -->
<div class="col-sm-12">
    {!! Form::label('id_solicitud_fondo', 'Id Solicitud Fondo:') !!}
    <p>{{ $pagoAprovado->id_solicitud_fondo }}</p>
</div>

<!-- Folio Anticipo Field -->
<div class="col-sm-12">
    {!! Form::label('folio_anticipo', 'Folio Anticipo:') !!}
    <p>{{ $pagoAprovado->folio_anticipo }}</p>
</div>

<!-- Observaciones Field -->
<div class="col-sm-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{{ $pagoAprovado->observaciones }}</p>
</div>

