<div class="table-responsive">
    <table class="table" id="solicitudFondos-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Folio</th>
                <th>Estacionamiento</th>
                <th>Tipo Solicitud</th>
                <th>Concepto</th>
                <th>Importe memo</th>
                <th>Fecha Ingreso Finanzas</th>
                {{-- <th>Fecha Entrega Cxp</th>
                <th>Fecha Ingreso Cxp</th> --}}
                <th>Estatus Registro</th>
                <th>Estatus Peticion</th>
                <th>Observaciones</th>
                <th>Procedencia</th>
                <th>Documento</th>
                <th>Aceptar/Rechazar</th>
                <th>Editar</th>
                <th>Eliminar</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($solicitudFondos as $solicitudFondo)
                <tr>
                    <td>{!! $solicitudFondo->id !!}</td>
                    <td>{!! $solicitudFondo->folio_documento !!}</td>
                    <td>{{ $solicitudFondo->id_estacionamiento }}</td>
                    <td>{{ $solicitudFondo->tipo_solicitud }}</td>
                    <td>{{ $solicitudFondo->concepto }}</td>
                    <td>{{ $solicitudFondo->importe_solicitado }}</td>
                    <td>{{ date('d-m-Y', strtotime($solicitudFondo->fecha_ingreso_finanzas)) }}</td>
                    {{-- <td>{{ $solicitudFondo->fecha_entrega_cxp }}</td>
                    <td>{{ $solicitudFondo->fecha_ingreso_cxp }}</td> --}}
                    <td>{{ $solicitudFondo->estatus_registro }}</td>
                    <td>{{ $solicitudFondo->estatus_peticion }}</td>
                    <td>{{ $solicitudFondo->observaciones }}</td>
                    <td>{{ $solicitudFondo->procedencia }}</td>
                    <td>
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                            data-target="#exampleModal{!! $solicitudFondo->id !!}">
                            Ver
                        </button>
                    </td>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal{!! $solicitudFondo->id !!}" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Documentos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @foreach ($solicitudFondo->comprobantes_gastos as $item)
                                        <ul>
                                            <li><a href="{{ url('pdfPen/' . $item->id_comprobantes) }}"
                                                    class='btn-floating btn-sm btn-blue-grey'>{!! $item->nombre !!}</a>
                                            </li>
                                        </ul>
                                    @endforeach
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td width="120">
                        <button type="button" class='btn btn-success btn-xs' data-toggle="modal"
                            data-target="#valida{!! $solicitudFondo->id !!}">
                            <i class="fas fa-check-circle"></i>
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="valida{!! $solicitudFondo->id !!}" tabindex="-1" role="dialog"
                            aria-labelledby="validaLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="validaLabel">Actualizar estatus</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="POST" action="{{ route('solicitudFondos.aprobar') }}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <select name="estatus_peticion" required>
                                                <option value="" selected>Seleccione un estatus*</option>
                                                <option value="Autoriza Sergio">Autoriza Sergio</option>
                                                <option value="Rechazado">Rechazado</option>
                                                <option value="Reingresado">Reingresado</option>
                                                <option value="N/A">N/A</option>
                                            </select>
                                            {!! Form::hidden('id', $solicitudFondo->id) !!}
                                            <br><br>
                                            <button type="submit" class="btn btn-primary">Enviar</button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    {!! Form::open(['route' => ['solicitudFondos.destroy', $solicitudFondo->id], 'method' => 'delete']) !!}
                    <td width="120">
                        <a href="{{ route('solicitudFondos.edit', [$solicitudFondo->id]) }}"
                            class='btn btn-primary btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                    </td>
                    <td width="120">
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </td>
                    {!! Form::close() !!}
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#solicitudFondos-table').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
        });
    </script>
@endsection
