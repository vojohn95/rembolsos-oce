<!-- Id Estacionamiento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_estacionamiento', 'Estacionamiento:') !!}
    <br>
    <select name="id_estacionamiento" id="id_estacionamiento" required>
        <option value="" >Seleccione una opción</option>
        @foreach ($estacionamientos as $est)
            <option value="{{$est->no_est}}" {{ ( $est->no_est == $solicitudFondo->id_estacionamiento) ? 'selected' : '' }}>({{$est->no_est}}) / {{$est->nombre}}</option>
        @endforeach
    </select>
</div>

<!-- Tipo Solicitud Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_solicitud', 'Tipo Solicitud:') !!}
    <br>
    {!! Form::select(
    'tipo_solicitud',
    ['' => 'Seleccione una opción', 'Anticipo' => 'Anticipo', 'Comprobación' => 'Comprobación', 'Reembolso' => 'Reembolso'],
    $solicitudFondo->tipo_solicitud,
    [
        'class' => 'form-control',
        'placeholder' => 'Seleccione un tipo de solicitud',
    ],
) !!}

</div>

<!-- Concepto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('concepto', 'Concepto:') !!}
    {!! Form::text('concepto', null, ['class' => 'form-control', 'maxlength' => 255, 'maxlength' => 255]) !!}
</div>

<!-- Importe Solicitado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('importe_solicitado', 'Importe Solicitado:') !!}
    {!! Form::number('importe_solicitado', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Ingreso Finanzas Field -->
<div class="form-group col-sm-6">
    @php
        $horaactual = Carbon\Carbon::now();
    @endphp
    {!! Form::label('fecha_ingreso_finanzas', 'Fecha Ingreso Finanzas:') !!}
    {!! Form::text('fecha_ingreso_finanzas', null, ['class' => 'form-control', 'id' => 'fecha_ingreso_finanzas', 'readonly']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#fecha_ingreso_finanzas').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

{!! Form::hidden('fecha_entrega_cxp', $horaactual->toDateTimeString(), ['class' => 'form-control', 'id' => 'fecha_entrega_cxp']) !!}

@push('page_scripts')
    <script type="text/javascript">
        $('#fecha_entrega_cxp').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Fecha Ingreso Cxp Field -->
{!! Form::hidden('fecha_ingreso_cxp', $horaactual->toDateTimeString(), ['class' => 'form-control', 'id' => 'fecha_ingreso_cxp']) !!}


@push('page_scripts')
    <script type="text/javascript">
            format: 'YYYY-MM-DD HH:mm:ss',
        $('#fecha_ingreso_cxp').datetimepicker({
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Estatus Registro Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estatus_registro', 'Estatus Registro:') !!}
    <br>
    {!! Form::select(
    'estatus_registro',
    ['Registrado' => 'Registrado', 'Capturado' => 'Capturado'],
    $solicitudFondo->estatus_registro,
    [
        'class' => 'form-control',
        'placeholder' => 'Seleccione un estatus',
    ],
) !!}
</div>

<!-- Estatus Peticion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estatus_peticion', 'Estatus Peticion:') !!}
    <br>
    {!! Form::select(
    'estatus_peticion',
    ['Autoriza Sergio' => 'Autoriza Sergio', 'Rechazado' => 'Rechazado', 'Reingresado' => 'Reingresado', 'N/A' => 'N/A'],
    $solicitudFondo->estatus_peticion,
    [
        'class' => 'form-control',
        'placeholder' => 'Seleccione un estatus',
    ],
) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control', 'maxlength' => 1000, 'maxlength' => 1000]) !!}
</div>
