@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Editar</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($solicitudFondo, ['route' => ['solicitudFondos.update', $solicitudFondo->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('solicitud_fondos.fieldsedit')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('solicitudFondos.index') }}" class="btn btn-default">Cancelar</a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
