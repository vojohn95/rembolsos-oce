<!-- Id Estacionamiento Field -->
<div class="col-sm-12">
    {!! Form::label('id_estacionamiento', 'Id Estacionamiento:') !!}
    <p>{{ $solicitudFondo->id_estacionamiento }}</p>
</div>

<!-- Tipo Solicitud Field -->
<div class="col-sm-12">
    {!! Form::label('tipo_solicitud', 'Tipo Solicitud:') !!}
    <p>{{ $solicitudFondo->tipo_solicitud }}</p>
</div>

<!-- Concepto Field -->
<div class="col-sm-12">
    {!! Form::label('concepto', 'Concepto:') !!}
    <p>{{ $solicitudFondo->concepto }}</p>
</div>

<!-- Importe Solicitado Field -->
<div class="col-sm-12">
    {!! Form::label('importe_solicitado', 'Importe Solicitado:') !!}
    <p>{{ $solicitudFondo->importe_solicitado }}</p>
</div>

<!-- Fecha Ingreso Finanzas Field -->
<div class="col-sm-12">
    {!! Form::label('fecha_ingreso_finanzas', 'Fecha Ingreso Finanzas:') !!}
    <p>{{ $solicitudFondo->fecha_ingreso_finanzas }}</p>
</div>

<!-- Fecha Entrega Cxp Field -->
<div class="col-sm-12">
    {!! Form::label('fecha_entrega_cxp', 'Fecha Entrega Cxp:') !!}
    <p>{{ $solicitudFondo->fecha_entrega_cxp }}</p>
</div>

<!-- Fecha Ingreso Cxp Field -->
<div class="col-sm-12">
    {!! Form::label('fecha_ingreso_cxp', 'Fecha Ingreso Cxp:') !!}
    <p>{{ $solicitudFondo->fecha_ingreso_cxp }}</p>
</div>

<!-- Estatus Registro Field -->
<div class="col-sm-12">
    {!! Form::label('estatus_registro', 'Estatus Registro:') !!}
    <p>{{ $solicitudFondo->estatus_registro }}</p>
</div>

<!-- Estatus Peticion Field -->
<div class="col-sm-12">
    {!! Form::label('estatus_peticion', 'Estatus Peticion:') !!}
    <p>{{ $solicitudFondo->estatus_peticion }}</p>
</div>

<!-- Observaciones Field -->
<div class="col-sm-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{{ $solicitudFondo->observaciones }}</p>
</div>

