<div class="table-responsive">
    <table class="table" id="gerentes-table">
        <thead>
        <tr>
            <th>Id Distritales</th>
        <th>Name</th>
        <th>Email</th>
        <th>Password</th>
        <th>Remember Token</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($gerentes as $gerente)
            <tr>
                <td>{{ $gerente->id_distritales }}</td>
            <td>{{ $gerente->name }}</td>
            <td>{{ $gerente->email }}</td>
            <td>{{ $gerente->password }}</td>
            <td>{{ $gerente->remember_token }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['gerentes.destroy', $gerente->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('gerentes.show', [$gerente->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('gerentes.edit', [$gerente->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
