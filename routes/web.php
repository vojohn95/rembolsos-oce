<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SolicitudFondoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\Auth\LoginController::class , 'showLoginForm']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('distritals', App\Http\Controllers\DistritalController::class);


Route::resource('estacionamientos', App\Http\Controllers\EstacionamientoController::class);


Route::resource('gerentes', App\Http\Controllers\GerenteController::class);


Route::resource('pagoAprovados', App\Http\Controllers\PagoAprovadoController::class);


Route::resource('solicitudFondos', App\Http\Controllers\SolicitudFondoController::class);

Route::post('solicitudFondos/aprobar', [App\Http\Controllers\SolicitudFondoController::class, 'aprobar'])->name('solicitudFondos.aprobar');

Route::get('/pdfPen/{id}', [App\Http\Controllers\SolicitudFondoController::class, 'downloadPdf'])->name('pdfPen');
Route::resource('users', App\Http\Controllers\UsersController::class);

