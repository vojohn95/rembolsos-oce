-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.33 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para fondosoperativos
CREATE DATABASE IF NOT EXISTS `fondosoperativos` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `fondosoperativos`;

-- Volcando estructura para tabla fondosoperativos.distritales
CREATE TABLE IF NOT EXISTS `distritales` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `clave_distrito` varchar(20) DEFAULT NULL,
  `estatus` tinyint(4) DEFAULT '1',
  `correo` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla fondosoperativos.distritales: ~0 rows (aproximadamente)
DELETE FROM `distritales`;
/*!40000 ALTER TABLE `distritales` DISABLE KEYS */;
INSERT INTO `distritales` (`id`, `nombre`, `clave_distrito`, `estatus`, `correo`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(12, 'test', 'test', 1, 'test@corre.cm', '2022-02-03 19:57:24', '2022-02-03 20:04:34', '2022-02-03 20:04:34');
/*!40000 ALTER TABLE `distritales` ENABLE KEYS */;

-- Volcando estructura para tabla fondosoperativos.estacionamientos
CREATE TABLE IF NOT EXISTS `estacionamientos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_estacionamiento` int(11) DEFAULT NULL,
  `nombre_proyecto` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `calle` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `no_ext` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `colonia` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `municipio` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `pais` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `cp` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `tipo_estacionamiento` char(1) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `id_gerentes` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `estatus` enum('A','E') COLLATE utf8mb4_spanish_ci DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_gerentes` (`id_gerentes`),
  CONSTRAINT `fk_gerentes` FOREIGN KEY (`id_gerentes`) REFERENCES `gerentes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci COMMENT='tabla de estacionamientos';

-- Volcando datos para la tabla fondosoperativos.estacionamientos: ~0 rows (aproximadamente)
DELETE FROM `estacionamientos`;
/*!40000 ALTER TABLE `estacionamientos` DISABLE KEYS */;
/*!40000 ALTER TABLE `estacionamientos` ENABLE KEYS */;

-- Volcando estructura para tabla fondosoperativos.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla fondosoperativos.failed_jobs: ~0 rows (aproximadamente)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla fondosoperativos.gerentes
CREATE TABLE IF NOT EXISTS `gerentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_distritales` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_distritales` (`id_distritales`),
  CONSTRAINT `fk_id_distritales` FOREIGN KEY (`id_distritales`) REFERENCES `distritales` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci COMMENT='Tabla de gerentes';

-- Volcando datos para la tabla fondosoperativos.gerentes: ~0 rows (aproximadamente)
DELETE FROM `gerentes`;
/*!40000 ALTER TABLE `gerentes` DISABLE KEYS */;
/*!40000 ALTER TABLE `gerentes` ENABLE KEYS */;

-- Volcando estructura para tabla fondosoperativos.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla fondosoperativos.migrations: ~4 rows (aproximadamente)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla fondosoperativos.pagos_aprobados
CREATE TABLE IF NOT EXISTS `pagos_aprobados` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_aprueba_usuarios` bigint(20) unsigned DEFAULT NULL,
  `tipo_pago` varchar(255) DEFAULT NULL COMMENT 'se captura el tipo de pago realizado',
  `monto_pago` decimal(20,2) DEFAULT NULL,
  `fecha_pago` date DEFAULT NULL,
  `id_estacionamiento_paga` int(11) DEFAULT NULL COMMENT 'el id del estacionamiento que está pagando',
  `id_solicitud_fondo` int(11) DEFAULT NULL,
  `folio_anticipo` varchar(255) DEFAULT NULL,
  `observaciones` varchar(1000) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_aprueba_pago` (`id_aprueba_usuarios`),
  KEY `id_solicitud_pago_fk` (`id_solicitud_fondo`),
  KEY `id_estacionamiento_pago_fk` (`id_estacionamiento_paga`),
  CONSTRAINT `id_aprueba_pago` FOREIGN KEY (`id_aprueba_usuarios`) REFERENCES `users` (`id`),
  CONSTRAINT `id_estacionamiento_pago_fk` FOREIGN KEY (`id_estacionamiento_paga`) REFERENCES `solicitud_fondos` (`id`),
  CONSTRAINT `id_solicitud_pago_fk` FOREIGN KEY (`id_solicitud_fondo`) REFERENCES `solicitud_fondos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla fondosoperativos.pagos_aprobados: ~0 rows (aproximadamente)
DELETE FROM `pagos_aprobados`;
/*!40000 ALTER TABLE `pagos_aprobados` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagos_aprobados` ENABLE KEYS */;

-- Volcando estructura para tabla fondosoperativos.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla fondosoperativos.password_resets: ~0 rows (aproximadamente)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla fondosoperativos.permisos
CREATE TABLE IF NOT EXISTS `permisos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla fondosoperativos.permisos: ~0 rows (aproximadamente)
DELETE FROM `permisos`;
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;

-- Volcando estructura para tabla fondosoperativos.personal_access_tokens
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla fondosoperativos.personal_access_tokens: ~0 rows (aproximadamente)
DELETE FROM `personal_access_tokens`;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;

-- Volcando estructura para tabla fondosoperativos.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_central_user` bigint(20) unsigned DEFAULT NULL,
  `id_permiso` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roles_id_central_user_foreign` (`id_central_user`),
  KEY `roles_id_permiso_foreign` (`id_permiso`),
  KEY `idx_rolesID` (`id`) USING BTREE,
  CONSTRAINT `roles_id_central_user_foreign` FOREIGN KEY (`id_central_user`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `roles_id_permiso_foreign` FOREIGN KEY (`id_permiso`) REFERENCES `permisos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla fondosoperativos.roles: ~0 rows (aproximadamente)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Volcando estructura para tabla fondosoperativos.solicitud_fondos
CREATE TABLE IF NOT EXISTS `solicitud_fondos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_estacionamiento` int(11) DEFAULT NULL,
  `tipo_solicitud` enum('Anticipo','Comprobación','Reembolso') DEFAULT NULL,
  `concepto` varchar(255) DEFAULT NULL COMMENT 'concepto solicitud',
  `importe_solicitado` decimal(20,2) DEFAULT NULL COMMENT 'importe solicitado de fondos',
  `fecha_ingreso_finanzas` date DEFAULT NULL COMMENT 'fecha ingreso a finanzas',
  `fecha_entrega_cxp` date DEFAULT NULL COMMENT 'se captura la fecha que se entrega a cxp',
  `fecha_ingreso_cxp` date DEFAULT NULL COMMENT 'se captura la fecha que ingresa a cxp',
  `estatus_registro` enum('Registrado','Capturado') DEFAULT NULL COMMENT 'el estado de registro de cxp',
  `estatus_peticion` enum('Autoriza Sergio','Rechazado','Reingresado','N/A') DEFAULT NULL COMMENT 'se usa para ver si fue rechazado o aceptado',
  `observaciones` varchar(1000) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_proyecto_fondo_fk` (`id_estacionamiento`),
  CONSTRAINT `id_proyecto_fondo_fk` FOREIGN KEY (`id_estacionamiento`) REFERENCES `estacionamientos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla fondosoperativos.solicitud_fondos: ~0 rows (aproximadamente)
DELETE FROM `solicitud_fondos`;
/*!40000 ALTER TABLE `solicitud_fondos` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitud_fondos` ENABLE KEYS */;

-- Volcando estructura para tabla fondosoperativos.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla fondosoperativos.users: ~2 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'john', 'john@gmail.com', NULL, '$2y$10$1y/lQ2YxFlyKNbGI6nFxi.J8QrFX9Cf/mDaDmE5eoEcHydCO7Tuf2', NULL, '2022-02-03 19:53:21', '2022-02-03 19:53:21', NULL),
	(3, 'tes', 'john@gmail.coma', '2022-02-03 14:08:44', 'adasdasdasd', NULL, '2022-02-03 20:08:55', '2022-02-03 20:08:55', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
