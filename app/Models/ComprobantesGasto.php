<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ComprobantesGasto
 * 
 * @property int $id_comprobantes
 * @property int|null $id_solicitud_fondos
 * @property string|null $documento
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $nombre
 * 
 * @property SolicitudFondo|null $solicitud_fondo
 *
 * @package App\Models
 */
class ComprobantesGasto extends Model
{
	use SoftDeletes;
	protected $table = 'comprobantes_gastos';
	protected $primaryKey = 'id_comprobantes';

	protected $casts = [
		'id_solicitud_fondos' => 'int'
	];

	protected $fillable = [
		'id_solicitud_fondos',
		'documento',
		'nombre'
	];

	public function solicitud_fondo()
	{
		return $this->belongsTo(SolicitudFondo::class, 'id_solicitud_fondos');
	}
}
