<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SolicitudFondo
 *
 * @property int $id
 * @property int|null $id_estacionamiento
 * @property string|null $tipo_solicitud
 * @property string|null $concepto
 * @property float|null $importe_solicitado
 * @property Carbon|null $fecha_ingreso_finanzas
 * @property Carbon|null $fecha_entrega_cxp
 * @property Carbon|null $fecha_ingreso_cxp
 * @property string|null $estatus_registro
 * @property string|null $estatus_peticion
 * @property string|null $observaciones
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $folio_documento
 *
 * @property Collection|ComprobantesGasto[] $comprobantes_gastos
 * @property Collection|PagosAprobado[] $pagos_aprobados
 *
 * @package App\Models
 */
class SolicitudFondo extends Model
{
	use SoftDeletes;
	protected $table = 'solicitud_fondos';

	protected $casts = [
		'id_estacionamiento' => 'int',
		'importe_solicitado' => 'float'
	];

	protected $dates = [
		'fecha_ingreso_finanzas',
		'fecha_entrega_cxp',
		'fecha_ingreso_cxp'
	];

	protected $fillable = [
		'id_estacionamiento',
		'tipo_solicitud',
		'concepto',
		'importe_solicitado',
		'fecha_ingreso_finanzas',
		'fecha_entrega_cxp',
		'fecha_ingreso_cxp',
		'estatus_registro',
		'estatus_peticion',
		'observaciones',
		'folio_documento',
        'procedencia',
	];

	public function comprobantes_gastos()
	{
		return $this->hasMany(ComprobantesGasto::class, 'id_solicitud_fondos');
	}

	public function pagos_aprobados()
	{
		return $this->hasMany(PagosAprobado::class, 'id_solicitud_fondo');
	}
}
