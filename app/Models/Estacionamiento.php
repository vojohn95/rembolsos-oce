<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Estacionamiento
 * @package App\Models
 * @version February 3, 2022, 8:00 pm UTC
 *
 * @property \App\Models\Gerente $idGerentes
 * @property \Illuminate\Database\Eloquent\Collection $solicitudFondos
 * @property integer $no_estacionamiento
 * @property string $nombre_proyecto
 * @property string $direccion
 * @property string $calle
 * @property string $no_ext
 * @property string $colonia
 * @property string $municipio
 * @property string $estado
 * @property string $pais
 * @property string $cp
 * @property string $tipo_estacionamiento
 * @property integer $id_gerentes
 * @property string $estatus
 */
class Estacionamiento extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'estacionamientos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'no_estacionamiento',
        'nombre_proyecto',
        'direccion',
        'calle',
        'no_ext',
        'colonia',
        'municipio',
        'estado',
        'pais',
        'cp',
        'tipo_estacionamiento',
        'id_gerentes',
        'estatus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'no_estacionamiento' => 'integer',
        'nombre_proyecto' => 'string',
        'direccion' => 'string',
        'calle' => 'string',
        'no_ext' => 'string',
        'colonia' => 'string',
        'municipio' => 'string',
        'estado' => 'string',
        'pais' => 'string',
        'cp' => 'string',
        'tipo_estacionamiento' => 'string',
        'id_gerentes' => 'integer',
        'estatus' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'no_estacionamiento' => 'nullable|integer',
        'nombre_proyecto' => 'nullable|string|max:255',
        'direccion' => 'nullable|string|max:255',
        'calle' => 'nullable|string|max:255',
        'no_ext' => 'nullable|string|max:255',
        'colonia' => 'nullable|string|max:255',
        'municipio' => 'nullable|string|max:255',
        'estado' => 'nullable|string|max:255',
        'pais' => 'nullable|string|max:255',
        'cp' => 'nullable|string|max:255',
        'tipo_estacionamiento' => 'nullable|string|max:1',
        'id_gerentes' => 'nullable|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
        'estatus' => 'nullable|string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idGerentes()
    {
        return $this->belongsTo(\App\Models\Gerente::class, 'id_gerentes');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function solicitudFondos()
    {
        return $this->hasMany(\App\Models\SolicitudFondo::class, 'id_estacionamiento');
    }
}
