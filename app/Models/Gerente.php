<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Gerente
 * @package App\Models
 * @version February 3, 2022, 8:01 pm UTC
 *
 * @property \App\Models\Distritale $idDistritales
 * @property \Illuminate\Database\Eloquent\Collection $estacionamientos
 * @property integer $id_distritales
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 */
class Gerente extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'gerentes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_distritales',
        'name',
        'email',
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_distritales' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_distritales' => 'nullable|integer',
        'name' => 'nullable|string|max:255',
        'email' => 'nullable|string|max:255',
        'password' => 'nullable|string|max:255',
        'remember_token' => 'nullable|string|max:100',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idDistritales()
    {
        return $this->belongsTo(\App\Models\Distritale::class, 'id_distritales');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function estacionamientos()
    {
        return $this->hasMany(\App\Models\Estacionamiento::class, 'id_gerentes');
    }
}
