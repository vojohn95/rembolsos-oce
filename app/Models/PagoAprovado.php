<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PagoAprovado
 * @package App\Models
 * @version February 3, 2022, 8:02 pm UTC
 *
 * @property \App\Models\User $idApruebaUsuarios
 * @property \App\Models\SolicitudFondo $idEstacionamientoPaga
 * @property \App\Models\SolicitudFondo $idSolicitudFondo
 * @property integer $id_aprueba_usuarios
 * @property string $tipo_pago
 * @property number $monto_pago
 * @property string $fecha_pago
 * @property integer $id_estacionamiento_paga
 * @property integer $id_solicitud_fondo
 * @property string $folio_anticipo
 * @property string $observaciones
 */
class PagoAprovado extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'pagos_aprobados';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_aprueba_usuarios',
        'tipo_pago',
        'monto_pago',
        'fecha_pago',
        'id_estacionamiento_paga',
        'id_solicitud_fondo',
        'folio_anticipo',
        'observaciones'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_aprueba_usuarios' => 'integer',
        'tipo_pago' => 'string',
        'monto_pago' => 'decimal:2',
        'fecha_pago' => 'date',
        'id_estacionamiento_paga' => 'integer',
        'id_solicitud_fondo' => 'integer',
        'folio_anticipo' => 'string',
        'observaciones' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_aprueba_usuarios' => 'nullable',
        'tipo_pago' => 'nullable|string|max:255',
        'monto_pago' => 'nullable|numeric',
        'fecha_pago' => 'nullable',
        'id_estacionamiento_paga' => 'nullable|integer',
        'id_solicitud_fondo' => 'nullable|integer',
        'folio_anticipo' => 'nullable|string|max:255',
        'observaciones' => 'nullable|string|max:1000',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idApruebaUsuarios()
    {
        return $this->belongsTo(\App\Models\User::class, 'id_aprueba_usuarios');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idEstacionamientoPaga()
    {
        return $this->belongsTo(\App\Models\SolicitudFondo::class, 'id_estacionamiento_paga');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idSolicitudFondo()
    {
        return $this->belongsTo(\App\Models\SolicitudFondo::class, 'id_solicitud_fondo');
    }
}
