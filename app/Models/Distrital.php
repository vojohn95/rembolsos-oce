<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Distrital
 * @package App\Models
 * @version February 3, 2022, 8:00 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $gerentes
 * @property string $nombre
 * @property string $clave_distrito
 * @property boolean $estatus
 * @property string $correo
 */
class Distrital extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'distritales';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'clave_distrito',
        'estatus',
        'correo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'clave_distrito' => 'string',
        'estatus' => 'boolean',
        'correo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'nullable|string|max:50',
        'clave_distrito' => 'nullable|string|max:20',
        'estatus' => 'nullable|boolean',
        'correo' => 'nullable|string|max:100',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function gerentes()
    {
        return $this->hasMany(\App\Models\Gerente::class, 'id_distritales');
    }
}
