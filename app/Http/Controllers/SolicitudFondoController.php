<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Models\PagoAprovado;
use Illuminate\Http\Request;
use App\Models\SolicitudFondo;
use App\Models\ComprobantesGasto;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AppBaseController;
use App\Repositories\SolicitudFondoRepository;
use App\Http\Requests\CreateSolicitudFondoRequest;
use App\Http\Requests\UpdateSolicitudFondoRequest;

class SolicitudFondoController extends AppBaseController
{
    /** @var SolicitudFondoRepository $solicitudFondoRepository*/
    private $solicitudFondoRepository;

    public function __construct(SolicitudFondoRepository $solicitudFondoRepo)
    {
        $this->solicitudFondoRepository = $solicitudFondoRepo;
    }

    /**
     * Display a listing of the SolicitudFondo.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $solicitudFondos = SolicitudFondo::with('comprobantes_gastos')->where('estatus_peticion', '!=', 'Autoriza Sergio')->orderByDesc('id')->get();

        return view('solicitud_fondos.index')
            ->with('solicitudFondos', $solicitudFondos);
    }

    /**
     * Show the form for creating a new SolicitudFondo.
     *
     * @return Response
     */
    public function create()
    {
        $estacionamientos = DB::connection('mysql2')->SELECT('SELECT * FROM parks');
        return view('solicitud_fondos.create', compact('estacionamientos'));
    }

    /**
     * Store a newly created SolicitudFondo in storage.
     *
     * @param CreateSolicitudFondoRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $solicitudFondo = $this->solicitudFondoRepository->create($input);

        Flash::success('Solicitud añadida.');

        return redirect(route('solicitudFondos.index'));
    }

    /**
     * Display the specified SolicitudFondo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $solicitudFondo = $this->solicitudFondoRepository->find($id);

        if (empty($solicitudFondo)) {
            Flash::error('Solicitud Fondo not found');

            return redirect(route('solicitudFondos.index'));
        }

        return view('solicitud_fondos.show')->with('solicitudFondo', $solicitudFondo);
    }

    /**
     * Show the form for editing the specified SolicitudFondo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $solicitudFondo = $this->solicitudFondoRepository->find($id);

        if (empty($solicitudFondo)) {
            Flash::error('Solicitud Fondo not found');

            return redirect(route('solicitudFondos.index'));
        }

        $estacionamientos = DB::connection('mysql2')->SELECT('SELECT * FROM parks');

        return view('solicitud_fondos.edit', compact('estacionamientos'))->with('solicitudFondo', $solicitudFondo);
    }

    /**
     * Update the specified SolicitudFondo in storage.
     *
     * @param int $id
     * @param UpdateSolicitudFondoRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $solicitudFondo = $this->solicitudFondoRepository->find($id);

        if (empty($solicitudFondo)) {
            Flash::error('Solicitud Fondo not found');

            return redirect(route('solicitudFondos.index'));
        }

        $solicitudFondo = $this->solicitudFondoRepository->update($request->all(), $id);

        Flash::success('Solicitud actualizado.');

        return redirect(route('solicitudFondos.index'));
    }

    /**
     * Remove the specified SolicitudFondo from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $solicitudFondo = $this->solicitudFondoRepository->find($id);

        if (empty($solicitudFondo)) {
            Flash::error('Solicitud Fondo not found');

            return redirect(route('solicitudFondos.index'));
        }

        $this->solicitudFondoRepository->delete($id);

        Flash::success('Solicitud eliminado.');

        return redirect(route('solicitudFondos.index'));
    }

    public function aprobar(Request $request)
    {
        $solicitudFondo = SolicitudFondo::find($request->id);
        $solicitudFondo->estatus_peticion = $request->estatus_peticion;
        $solicitudFondo->save();

        $pagoaprobado = PagoAprovado::create([
            'id_aprueba_usuarios' => Auth::user()->id,
            'tipo_pago' => 'Pendiente de actualización',
            'monto_pago' => '0',
            'fecha_pago' => null,
            'id_estacionamiento_paga' => $solicitudFondo->id_estacionamiento,
            'id_solicitud_fondo' => $solicitudFondo->id,
            'folio_anticipo' => $solicitudFondo->solicitud_documentos,
            'observaciones' => $solicitudFondo->observaciones,
        ]);

        Flash::success('Solicitud con estatus actualizado.');

        return redirect(route('solicitudFondos.index'));
    }

    public function downloadPdf($id)
    {
        $pdf = ComprobantesGasto::find($id);
        $archivo = $pdf->documento;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $pdf->id . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=$pdf->nombre");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }
}
