<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AppBaseController;
use App\Repositories\PagoAprovadoRepository;
use App\Http\Requests\CreatePagoAprovadoRequest;
use App\Http\Requests\UpdatePagoAprovadoRequest;

class PagoAprovadoController extends AppBaseController
{
    /** @var PagoAprovadoRepository $pagoAprovadoRepository*/
    private $pagoAprovadoRepository;

    public function __construct(PagoAprovadoRepository $pagoAprovadoRepo)
    {
        $this->pagoAprovadoRepository = $pagoAprovadoRepo;
    }

    /**
     * Display a listing of the PagoAprovado.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pagoAprovados = $this->pagoAprovadoRepository->all();

        return view('pago_aprovados.index')
            ->with('pagoAprovados', $pagoAprovados);
    }

    /**
     * Show the form for creating a new PagoAprovado.
     *
     * @return Response
     */
    public function create()
    {

        return view('pago_aprovados.create');
    }

    /**
     * Store a newly created PagoAprovado in storage.
     *
     * @param CreatePagoAprovadoRequest $request
     *
     * @return Response
     */
    public function store(CreatePagoAprovadoRequest $request)
    {
        $input = $request->all();

        $pagoAprovado = $this->pagoAprovadoRepository->create($input);

        Flash::success('Pago Aprovado saved successfully.');

        return redirect(route('pagoAprovados.index'));
    }

    /**
     * Display the specified PagoAprovado.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pagoAprovado = $this->pagoAprovadoRepository->find($id);

        if (empty($pagoAprovado)) {
            Flash::error('Pago Aprovado not found');

            return redirect(route('pagoAprovados.index'));
        }

        return view('pago_aprovados.show')->with('pagoAprovado', $pagoAprovado);
    }

    /**
     * Show the form for editing the specified PagoAprovado.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pagoAprovado = $this->pagoAprovadoRepository->find($id);

        if (empty($pagoAprovado)) {
            Flash::error('Pago Aprovado not found');

            return redirect(route('pagoAprovados.index'));
        }

        return view('pago_aprovados.edit')->with('pagoAprovado', $pagoAprovado);
    }

    /**
     * Update the specified PagoAprovado in storage.
     *
     * @param int $id
     * @param UpdatePagoAprovadoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePagoAprovadoRequest $request)
    {
        $pagoAprovado = $this->pagoAprovadoRepository->find($id);

        if (empty($pagoAprovado)) {
            Flash::error('Pago Aprovado not found');

            return redirect(route('pagoAprovados.index'));
        }

        $pagoAprovado = $this->pagoAprovadoRepository->update($request->all(), $id);

        Flash::success('Pago Aprovado updated successfully.');

        return redirect(route('pagoAprovados.index'));
    }

    /**
     * Remove the specified PagoAprovado from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pagoAprovado = $this->pagoAprovadoRepository->find($id);

        if (empty($pagoAprovado)) {
            Flash::error('Pago Aprovado not found');

            return redirect(route('pagoAprovados.index'));
        }

        $this->pagoAprovadoRepository->delete($id);

        Flash::success('Pago Aprovado deleted successfully.');

        return redirect(route('pagoAprovados.index'));
    }
}
