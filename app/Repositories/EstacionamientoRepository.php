<?php

namespace App\Repositories;

use App\Models\Estacionamiento;
use App\Repositories\BaseRepository;

/**
 * Class EstacionamientoRepository
 * @package App\Repositories
 * @version February 3, 2022, 8:00 pm UTC
*/

class EstacionamientoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'no_estacionamiento',
        'nombre_proyecto',
        'direccion',
        'calle',
        'no_ext',
        'colonia',
        'municipio',
        'estado',
        'pais',
        'cp',
        'tipo_estacionamiento',
        'id_gerentes',
        'estatus'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Estacionamiento::class;
    }
}
