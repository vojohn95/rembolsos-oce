<?php

namespace App\Repositories;

use App\Models\SolicitudFondo;
use App\Repositories\BaseRepository;

/**
 * Class SolicitudFondoRepository
 * @package App\Repositories
 * @version February 3, 2022, 8:03 pm UTC
*/

class SolicitudFondoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_estacionamiento',
        'tipo_solicitud',
        'concepto',
        'importe_solicitado',
        'fecha_ingreso_finanzas',
        'fecha_entrega_cxp',
        'fecha_ingreso_cxp',
        'estatus_registro',
        'estatus_peticion',
        'observaciones'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SolicitudFondo::class;
    }
}
