<?php

namespace App\Repositories;

use App\Models\Distrital;
use App\Repositories\BaseRepository;

/**
 * Class DistritalRepository
 * @package App\Repositories
 * @version February 3, 2022, 8:00 pm UTC
*/

class DistritalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'clave_distrito',
        'estatus',
        'correo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Distrital::class;
    }
}
