<?php

namespace App\Repositories;

use App\Models\Gerente;
use App\Repositories\BaseRepository;

/**
 * Class GerenteRepository
 * @package App\Repositories
 * @version February 3, 2022, 8:01 pm UTC
*/

class GerenteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_distritales',
        'name',
        'email',
        'password',
        'remember_token'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Gerente::class;
    }
}
