<?php

namespace App\Repositories;

use App\Models\PagoAprovado;
use App\Repositories\BaseRepository;

/**
 * Class PagoAprovadoRepository
 * @package App\Repositories
 * @version February 3, 2022, 8:02 pm UTC
*/

class PagoAprovadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_aprueba_usuarios',
        'tipo_pago',
        'monto_pago',
        'fecha_pago',
        'id_estacionamiento_paga',
        'id_solicitud_fondo',
        'folio_anticipo',
        'observaciones'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PagoAprovado::class;
    }
}
